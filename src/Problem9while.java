import java.util.Scanner;

public class Problem9while {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            int i = 1;
            int n;
            System.out.print("Please input number: ");
            n = sc.nextInt();
            while (i < n+1) {
                int j = 1;
                while (j < n+1) {
                    System.out.print(j);
                    j++;
                }
                System.out.println();
                i++;
            }
        }
    }
}
